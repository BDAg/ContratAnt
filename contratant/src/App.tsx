import 'react-native-gesture-handler';
import React, {useReducer, useEffect, useMemo} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Register from './screens/Register/Register';
import Register2 from './screens/Register/Register2';
import Register3 from './screens/Register/Register3';
import Home from './screens/Home/Home';
import Profile from './screens/Profile/Profile';
import Login from './screens/Login/Login';
import {AuthContext} from './context/authContext';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from './screens/SplashScreen/SplashScreen';
import Filter from './screens/Filter/Filter';

export default function App({navigation}) {
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    },
  );

  useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        console.log('deu ruim pegando o token');
      }

      // After restoring token, we may need to validate it in production apps

      dispatch({type: 'RESTORE_TOKEN', token: userToken});
    };

    bootstrapAsync();
  }, []);

  const authContext = useMemo(
    () => ({
      signIn: async (data) => {
        //aqui vai ficar a funcao que vai ser chamada quando usuario logar

        dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
      },
      signOut: () => dispatch({type: 'SIGN_OUT'}),
      signUp: async (data) => {
        //aqui vai ser a funcao que vai chamar quando o usuario for criar uma conta

        dispatch({type: 'SIGN_IN', token: 'dummy-auth-token'});
      },
    }),
    [],
  );

  const Stack = createStackNavigator();
  if (state.isLoading) {
    return <SplashScreen />;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          {state.userToken === null ? (
            <Stack.Screen
              component={Home}
              name="Home"
              options={{headerShown: false}}
            />
          ) : (
            <>
              <Stack.Screen
                component={Register2}
                name="Login"
                options={{headerShown: false}}
              />
              <Stack.Screen
                component={Filter}
                name="Filter"
                options={{headerShown: true}}
              />
              <Stack.Screen
                component={Register}
                name="Register"
                options={{headerShown: false}}
              />
              <Stack.Screen
                component={Register2}
                name="Register2"
                options={{headerShown: false}}
              />
              <Stack.Screen
                component={Register3}
                name="Register3"
                options={{headerShown: false}}
              />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
