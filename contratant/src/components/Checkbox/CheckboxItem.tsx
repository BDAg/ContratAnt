import React, {useState} from 'react';
import {Text} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {styles} from '../../screens/Register/style';

const CheckboxItem = ({value, addItem, removeItem, profissoes, disabled}) => {
  const [checked, setChecked] = useState(false);

  return (
    <>
      <CheckBox
        disabled={disabled}
        value={checked}
        onValueChange={(newValue) => {
          checked ? removeItem(value) : addItem(value);
          setChecked(newValue);
        }}
      />
      <Text style={styles.TextoCheck}>{value}</Text>
    </>
  );
};

export default CheckboxItem;
