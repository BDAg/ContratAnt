import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: 'white',
  },
  rowll: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowAtividades: {
    // width: '45%',
    flexDirection: 'row',
  },
  itemAtividades: {paddingRight: 80},
  image: {
    height: 80,
    width: 80,
    borderRadius: 40,
    backgroundColor: 'black',
  },
  name: {
    fontSize: 24,
    marginLeft: 15,
  },
  user: {
    fontSize: 16,
    marginLeft: 25,
    marginTop: 10,
  },
  descricao: {
    fontSize: 24,
    color: '#C90101',
    fontWeight: 'bold',
    marginTop: 30,
  },
  descricao2: {
    paddingTop: 14,
    paddingBottom: 14,
    fontSize: 24,
    color: '#C90101',
    fontWeight: 'bold',
    // marginTop: 15,
  },
  sobre: {
    fontSize: 16,
    color: 'black',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#C90101',
  },
  bottomRedLine: {
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#C90101',
  },
  atividadesText: {
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    color: 'black',
  },
  editar: {
    height: 50,
    marginTop: 20,
    alignSelf: 'center',
    backgroundColor: '#C90404',
    padding: 10,
    borderRadius: 10,
    width: 150,
  },
  textEdit: {
    flex: 1,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  listinha: {
    // fontSize: 16,
    color: 'black',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#C90101',
    // alignContent: 'space-around',
  },
  title: {
    // fontSize: 30,
  },
  rowl: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  icon: {
    marginRight: 10,
  },
  contacts: {
    fontSize: 18,
  },
  rowl2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
});
