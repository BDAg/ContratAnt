import React from 'react';
import {View, Text} from 'react-native';

export const ItemList = ({title}) => (
  <View>
    <Text>{title}</Text>
  </View>
);
