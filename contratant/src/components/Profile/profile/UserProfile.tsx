import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import {styles} from './style';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function UserProfile({navigation}) {
  const DATA = [
    {
      id: '1',
      title: 'Pedreiro',
    },
    {
      id: '2',
      title: 'Encanador',
    },
    {
      id: '3',
      title: 'Pintor',
    },
    {
      id: '4',
      title: 'Modelo',
    },
    {
      id: '5',
      title: 'Programador PHP',
    },
  ];

  return (
    <>
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.rowll}>
            <Image
              style={styles.image}
              source={{uri: 'https://i.imgur.com/5CDwA7z.png'}}
            />
            <View>
              <Text style={styles.name}>PhPedro Catins29</Text>
              <Text style={styles.user}>@PhPedreiro</Text>
            </View>
          </View>
          <Text style={styles.descricao}>Descrição</Text>
          <Text style={styles.sobre}>
            Mussum Ipsum, cacilds vidis litro abertis. Si u mundo tá muito
            paradis? Toma um mé que o mundo vai girarzis! Posuere libero varius.
            Nullam a nisl ut ante blandit hendrerit. Aenean sit amet nisi.
          </Text>
          <Text style={styles.descricao2}>Atividades</Text>
          <View style={[styles.rowAtividades, styles.bottomRedLine]}>
            <View style={styles.itemAtividades}>
              <Text style={styles.atividadesText}>Pedreiro</Text>
              <Text style={styles.atividadesText}>Encanador</Text>
              <Text style={styles.atividadesText}>Pintor</Text>
            </View>
            <View style={styles.itemAtividades}>
              <Text style={styles.atividadesText}>Modelo</Text>
              <Text style={styles.atividadesText}>Programador PHP</Text>
            </View>
          </View>

          <Text style={styles.descricao2}>Contato</Text>
          <View style={styles.rowl}>
            <Icon
              style={styles.icon}
              name="envelope"
              size={20}
              color="#C90101"
              solid
            />
            <Text style={styles.contacts}> phpedro@life.com</Text>
          </View>
          <View style={styles.rowl2}>
            <Icon
              style={styles.icon}
              name="whatsapp"
              size={20}
              color="#C90101"
              solid
            />
            <Text style={styles.contacts}> 14 9 9898 1415</Text>
          </View>
          <View style={styles.rowl}>
            <Icon
              style={styles.icon}
              name="map-marker"
              size={20}
              color="#C90101"
              light
            />
            <Text style={styles.contacts}> Santarém - PA</Text>
          </View>
          {/* <Pressable
            style={styles.editar}
            onPress={() => navigation.navigate('Home')}>
            <Text style={styles.textEdit}>Editar</Text>
          </Pressable> */}
        </ScrollView>
      </View>
    </>
  );
}
