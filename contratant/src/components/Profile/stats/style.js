import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    color: '#C90101',
    fontWeight: 'bold',
    marginTop: 30,
  },
});
