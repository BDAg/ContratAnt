import React from 'react';
import {View, Text, FlatList, SafeAreaView} from 'react-native';
import {ItemList} from '../profile/ItemList';
import {styles} from './style';

export default function UserStats() {
  const DATA = [
    {
      id: '1',
      title: 'Pedreiro',
    },
    {
      id: '2',
      title: 'Encanador',
    },
    {
      id: '3',
      title: 'Pintor',
    },
    {
      id: '4',
      title: 'Modelo',
    },
    {
      id: '5',
      title: 'Programador PHP',
    },
  ];

  const renderItem = ({item}) => <ItemList title={item.title} />;

  return (
    <View>
      <Text style={styles.title}>Histórico de Visitas</Text>
    </View>
    // <View>
    //   <SafeAreaView>
    //     <FlatList
    //       style={styles.listinha}
    //       data={DATA}
    //       renderItem={renderItem}
    //       keyExtractor={item => item.id}
    //       horizontal={false}
    //       numColumns={2}
    //     />
    //   </SafeAreaView>
    // </View>
  );
}
