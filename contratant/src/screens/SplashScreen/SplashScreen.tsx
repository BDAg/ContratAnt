import React from 'react';
import {View, Image} from 'react-native';

const SplashScreen = () => {
  return (
    <View>
      <Image
        style={{width: 50, height: 50}}
        source={{uri: 'https://imgur.com/ob867g0'}}
      />
    </View>
  );
};

export default SplashScreen;
