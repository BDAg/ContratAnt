import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import UserStats from '../../components/Profile/stats/UserStats';
import UserProfile from '../../components/Profile/profile/UserProfile';

const Profile = ({navigation}) => {
  const Tab = createMaterialTopTabNavigator();
  return (
    <UserProfile />
    // <Tab.Navigator>
    //   <Tab.Screen name="Perfil" component={UserProfile} />
    //   <Tab.Screen name="Estatistica" component={UserStats} />
    // </Tab.Navigator>
  );
};

export default Profile;
