import React, {useState} from 'react';
import CheckBox from '@react-native-community/checkbox';
import {View, Text, ScrollView, Pressable} from 'react-native';
import {styles} from './style';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CheckboxItem from '../../components/Checkbox/CheckboxItem';

const Register2 = ({navigation, route}) => {
  const [search, setSearch] = useState('');
  const [Confirm, setConfirm] = useState(false);
  const [isSelected, setSelection] = useState(false);
  const [disabled, setDisabled] = useState(false);

  const [values, setValues] = useState([]);
  console.log(values);

  const onChange = (value) => {
    values.length <= 3 ? setValues([...values, value]) : setDisabled(true);
  };

  const removeItem = (item) => {
    setValues(values.filter((value) => value !== item));
  };

  /*const Verf = () => {
        if () {
          navigation.navigate('Register3')
        }else{
          setConfirm(true)
        }
    }*/

  return (
    <View style={styles.container} behavior="height" enabled>
      <ScrollView>
        <Text style={styles.comoAsPessoas3}>
          Aqui é para facilitar que os contratantes te encontrem
        </Text>

        <View style={styles.espaco1}></View>

        <View>
          <Text style={styles.escolhaCinco}>Escolha até 5 atividades</Text>
        </View>

        <View style={styles.rowGroup2}>
          <ScrollView style={{borderWidth: 0, width: 300}}>
            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Pedreiro(a)'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Encanador(a)'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Pintor(a)'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Eletricista'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Jardineiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Doméstica'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Técnico em Informática'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Marceneiro(a)'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Mecânico(a)'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Motorista'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Montador de móveis'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Carreteiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Técnico hidráulico'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Cuidador(a) de idosos'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Diarista'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Servente de pedreiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Gesseiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Vidraceiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Chaveiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Costureira'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Decorador'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Motoboy'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Serralheiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Babá'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Churrasqueiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Caseira'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Caseiro'}
                addItem={onChange}
                removeItem={removeItem}
              />
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Cozinheira'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <CheckboxItem
                disabled={disabled}
                profissoes={values}
                value={'Faxineira'}
                addItem={onChange}
                removeItem={removeItem}
              />
            </View>
          </ScrollView>
        </View>

        <View style={styles.Paginador2}>
          <View style={styles.CircleShapeView2} />
          <View style={styles.CircleShapeView} />
          <View style={styles.CircleShapeView2} />
        </View>

        <View style={styles.rowzinha2}>
          <Pressable
            onPress={() => navigation.navigate('Register')}
            style={styles.filter}>
            <Text style={styles.voltar}>Anterior</Text>
          </Pressable>

          <Pressable
            style={styles.banner}
            onPress={() => navigation.navigate('Register3')}>
            <Text style={styles.text}>Próximo</Text>
          </Pressable>
        </View>
      </ScrollView>
    </View>
  );
};
export default Register2;
