import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Pressable,
} from 'react-native';
import FormField from './../components/FormField';
import {styles} from './style';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Register = ({navigation}) => {
  const [search, setSearch] = useState('');

  const [Confirm, setConfirm] = useState(false);
  const [Nome, setNome] = useState('');
  const [Username, setUsername] = useState('');
  const [Email, setEmail] = useState('');
  const [Senha, setSenha] = useState('');
  const [ConfirmarSenha, setConfirmarSenha] = useState('');

  const Verf = () => {
    if (
      Nome !== '' &&
      Username !== '' &&
      Email !== '' &&
      Senha !== '' &&
      ConfirmarSenha !== '' &&
      Senha === ConfirmarSenha
    ) {
      navigation.navigate('Register2');
    } else {
      setConfirm(true);
    }
  };

  const Comfir = () => {
    setConfirm(false);
  };

  return (
    <View Style={styles.container} behavior="height" enabled>
      <ScrollView>
        <Text style={styles.comoAsPessoas3}>
          Como as pessoas podem te identificar?
        </Text>
        <View style={styles.rowGroup}>
          <TextInput
            style={Object.assign(
              {},
              styles.textNome,
              Nome === '' && Confirm === true ? styles.error1 : null,
            )}
            // value={search}
            // onChangeText={e => setSearch(e)}
            placeholder="Nome"
            padding={10}
            onChangeText={(Value) => setNome(Value)}
          />

          <TextInput
            style={Object.assign(
              {},
              styles.textUsername,
              Username === '' && Confirm === true ? styles.error1 : null,
            )}
            // value={search}
            // onChangeText={e => setSearch(e)}
            placeholder="Username"
            padding={10}
            onChangeText={(Value) => setUsername(Value)}
          />
          <TextInput
            style={Object.assign(
              {},
              styles.textInput,
              Email === '' && Confirm === true ? styles.error1 : null,
            )}
            // value={search}
            // onChangeText={e => setSearch(e)}
            placeholder="Email"
            padding={10}
            onChangeText={(Value) => setEmail(Value)}
          />
          <TextInput
            style={Object.assign(
              {},
              styles.textInput,
              (Senha === '' || Senha !== ConfirmarSenha) && Confirm === true
                ? styles.error1
                : null,
            )}
            // value={search}
            // onChangeText={e => setSearch(e)}
            placeholder="Senha"
            padding={10}
            secureTextEntry={true}
            onChangeText={(Value) => setSenha(Value)}
          />
          <TextInput
            style={Object.assign(
              {},
              styles.textInput,
              (ConfirmarSenha === '' || Senha !== ConfirmarSenha) &&
                Confirm === true
                ? styles.error1
                : null,
            )}
            // value={search}
            // onChangeText={e => setSearch(e)}
            placeholder="Confirmar Senha"
            padding={10}
            secureTextEntry={true}
            onChangeText={(Value) => setConfirmarSenha(Value)}
          />
        </View>

        <View style={styles.Paginador}>
          <View style={styles.CircleShapeView} />
          <View style={styles.CircleShapeView2} />
          <View style={styles.CircleShapeView2} />
        </View>

        <View style={styles.rowzinha}>
          <Text style={styles.voltar}>Anterior</Text>
          <Pressable style={styles.banner} onPress={() => Verf()}>
            <Text style={styles.text}>Próximo</Text>
          </Pressable>
        </View>
      </ScrollView>
    </View>
  );
};

export default Register;
