import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Pressable,
  StyleSheet,
  Alert,
} from 'react-native';
import {styles} from './style';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {launchImageLibrary} from 'react-native-image-picker';

const Register3 = ({navigation}) => {
  const [search, setSearch] = useState('');

  const [Confirm, setConfirm] = useState(false);
  const [Telefone, setTelefone] = useState('');
  const [Cidade, setCidade] = useState('');
  const [UF, setUF] = useState('');

  const [photo, setPhoto] = useState({
    uri:
      'https://e7.pngegg.com/pngimages/865/715/png-clipart-computer-icons-user-avatar-management-add-icon-smiley-business.png',
  });

  const Verf = () => {
    if (Telefone !== '' && Cidade !== '' && UF !== '') {
      Alert.alert(
        'Cadastro concluido',
        '',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );

      //navigation.navigate('Register2')
    } else {
      setConfirm(true);
    }
  };

  const openLibrary = () => {
    launchImageLibrary(
      {mediaType: 'photo', includeBase64: true},
      (response) => {
        console.log('Response = ', response.base64);
      },
    );
  };

  const chooseImage = () => {
    Alert.alert(
      'Escolher imagem',
      '',
      [
        {
          text: 'Camera',
          onPress: () => console.log('camera'),
        },
        {text: 'Galeria de fotos', onPress: () => openLibrary()},
      ],
      {cancelable: false},
    );
  };

  return (
    <View Style={styles.container} behavior="height" enabled>
      <ScrollView>
        <Text style={styles.comoAsPessoas3}>Sobre você</Text>

        <Pressable style={styles.imag3} onPress={() => chooseImage()}>
          <Image style={styles.image} source={photo} />
        </Pressable>

        <View>
          <Text style={styles.escolhaCinco}>Sobre você</Text>
        </View>

        <View style={styles.fale3}>
          <TextInput
            style={styles.textInputfale3}
            //value={search}
            //onChangeText={e => setSearch(e)}
            placeholder="Fale um pouco sobre você"
            padding={10}
            multiline={true}
            numberOfLines={8}
          />
        </View>

        <View style={styles.rowGroup3}>
          <TextInput
            style={Object.assign(
              {},
              styles.textInput,
              Telefone === '' && Confirm === true ? styles.error1 : null,
            )}
            //value={search}
            //onChangeText={e => setSearch(e)}
            placeholder="Telefone"
            padding={10}
            onChangeText={(Value) => setTelefone(Value)}
          />
        </View>

        <View style={styles.rowGroupCidadeUF}>
          <View style={{width: 50}}>
            <TextInput
              style={styles.textInputcidade}
              style={Object.assign(
                {},
                styles.textInputcidade,
                Cidade === '' && Confirm === true ? styles.error1 : null,
              )}
              //value={search}
              //onChangeText={e => setSearch(e)}
              placeholder="Cidade"
              padding={10}
              onChangeText={(Value) => setCidade(Value)}
            />
          </View>
          <View style={{width: 60}}>
            <TextInput
              style={styles.textInputuf}
              style={Object.assign(
                {},
                styles.textInputuf,
                UF === '' && Confirm === true ? styles.error1 : null,
              )}
              //value={search}
              //onChangeText={e => setSearch(e)}
              placeholder="UF"
              padding={10}
              onChangeText={(Value) => setUF(Value)}
            />
          </View>
        </View>

        <View style={styles.Paginador3}>
          <View style={styles.CircleShapeView2} />
          <View style={styles.CircleShapeView2} />
          <View style={styles.CircleShapeView} />
        </View>

        <View style={styles.rowzinha3}>
          <Pressable onPress={() => navigation.navigate('Register2')}>
            <Text style={styles.voltar}>Anterior</Text>
          </Pressable>

          <Pressable style={styles.banner3} onPress={() => Verf()}>
            <Text style={styles.text}>Finalizar cadastro</Text>
          </Pressable>
        </View>
      </ScrollView>
    </View>
  );
};
export default Register3;
