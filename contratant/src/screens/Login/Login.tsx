import React, { useState } from 'react';
import { View, Text, Image, TextInput, Pressable, Alert } from 'react-native';
import { styles } from './style';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Login = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const Verf = () => {
    if (email === '' || password === '') {
      Alert.alert(
        'Login Invalido',
        `${email === '' ? `Email invalido\n` : ''}${password === '' ? `Senha invalida` : ''}`,
        [
          {text: 'OK'},
        ],
        {cancelable: false},
      );
    }else {
      console.log('Entrar OK!')
    }
  }

  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={{ uri: 'https://i.imgur.com/ob867g0.png' }}
      />
      <View style={styles.rowGroup}>
        <Icon style={styles.icon} name="user" size={20} color="#C90101" solid />
        <TextInput
          placeholder="Usuario"
          style={styles.textInput}
          value={email}
          onChangeText={e => setEmail(e)}
        />
      </View>
      <View style={styles.rowGroup}>
        <Icon style={styles.icon} name="lock" size={20} color="#C90101" solid />
        <TextInput
          secureTextEntry={showPassword}
          placeholder="Senha"
          style={styles.textInput}
          value={password}
          onChangeText={e => setPassword(e)}
        />
        <Text
          style={styles.showPassword}
          onPress={() => setShowPassword(!showPassword)}>
          Mostrar
        </Text>
        <Text
          style={styles.forgotPassword}
          onPress={() => navigation.navigate('Register')}>   
            Esqueceu sua senha?
        </Text>
      </View>
      <Pressable
        style={styles.confirm}
        onPress={() => Verf()}>
        <Text style={styles.text}>
          Entrar
        </Text>
      </Pressable>
      <View style={styles.footer}>
        <Text style={styles.noAccount}>
          Não tem uma conta?
        </Text>
        <Text 
          style={styles.link}
          onPress={() => navigation.navigate('Register')}>
          Cadastre-se agora!
        </Text>
      </View>
    </View>
  );
};

export default Login;
