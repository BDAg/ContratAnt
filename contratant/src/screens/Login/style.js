import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    justifyContent: 'center',
  },
  image: {
    // marginTop: 48,
    resizeMode: 'contain',
    height: 100,
    width: '100%',
  },
  textInput: {
    paddingLeft: 32,
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#282828',
  },
  rowGroup: {
    marginTop: 32,
    alignItems: 'center',
    flexDirection: 'row',
  },
  icon: {
    position: 'absolute',
  },
  showPassword: {
    fontSize: 18,
    color: '#C90101',
    position: 'absolute',
    right: 15,
  },
  confirm: {
    marginTop: 93,
    width: '100%',
    height: 56,
    backgroundColor: '#C90101',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 23,
    fontWeight: 'bold',
    marginRight: 10,
  },
  forgotPassword: {
    fontSize: 14,
    opacity: 0.5,
    position: 'absolute',
    right: 15,
    top: 60,
  },
  footer: {
    marginTop: 14,
    width: '100%',
    alignItems: 'center',
  },
  noAccount: {
    fontSize: 18,
    opacity: 0.5,
  },
  link: {
    fontSize: 18,
    opacity: 0.5,
    color: '#C90101',
    textDecorationLine: 'underline',
  },
});
