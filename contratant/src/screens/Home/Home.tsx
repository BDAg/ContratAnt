import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  SafeAreaView,
  ScrollView,
  Pressable,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {styles} from './style';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {AuthContext} from '../../context/authContext';

import Fake from './FilterfakeValues';

const Home = ({navigation}) => {
  const [city, setCity] = useState('null');
  const [work, setWork] = useState('null');
  const [search, setSearch] = useState('');
  const {authState} = React.useContext(AuthContext);
  const [filterUnPress, setfilterUnPress] = useState(true);

  let citys = Fake.citys.map(({label, value}) => {
    return <Picker.Item key={value} label={label} value={value} />;
  });

  let works = Fake.works.map(({label, value}) => {
    return <Picker.Item key={value} label={label} value={value} />;
  });

  const [authConfirm] = useState(false);

  return (
    <SafeAreaView>
      <ScrollView>
        {authState.isSignout ? (
          <Pressable
            style={styles.user}
            onPress={() => navigation.navigate('Login')}>
            <Image
              style={styles.userImage}
              source={{uri: 'https://i.imgur.com/uOnc8JS.jpg'}}
            />
          </Pressable>
        ) : null}
        <View style={styles.container}>
          <Image
            style={styles.image}
            source={{uri: 'https://i.imgur.com/5CDwA7z.png'}}
          />
          <Text style={styles.title}>Que profissional você procura?</Text>
          <View style={styles.rowGroup}>
            <Icon
              style={styles.icon}
              name="search"
              size={22}
              color="#C90101"
              solid
            />
            <TextInput
              style={styles.textInput}
              value={search}
              onChangeText={(e) => setSearch(e)}
            />
            <Pressable
              onPress={() => setfilterUnPress(!filterUnPress)}
              style={styles.filter}>
              <Icon name="align-right" size={22} color="#C90101" solid />
            </Pressable>
          </View>
          <View
            style={Object.assign(
              {},
              styles.filterHandler,
              filterUnPress ? styles.filterHandlerUnPress : null,
            )}>
            <Picker
              selectedValue={city}
              style={styles.picker}
              onValueChange={(itemValue) => setCity(itemValue)}>
              <Picker.Item key="null" label="Todas as Cidades" value="null" />
              {citys}
            </Picker>
            <Picker
              selectedValue={work}
              style={styles.picker}
              onValueChange={(itemValue) => setWork(itemValue)}>
              <Picker.Item
                key="null"
                label="Todas as Profissões"
                value="null"
              />
              {works}
            </Picker>
          </View>
          {!authState.isSignout ? (
            <Pressable
              style={styles.banner}
              onPress={() => navigation.navigate('Register')}>
              <Text style={styles.text}>
                Faça parte da nossa lista de profissionais.
              </Text>
              <Icon name="tools" size={42} color="#FFFFFF" solid />
            </Pressable>
          ) : null}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
