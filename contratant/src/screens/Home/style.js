import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    marginTop: 130,
  },
  user: {
    position: 'absolute',
    top: 32,
    right: 32,
    height: 48,
    width: 48,
    alignSelf: 'flex-end',
    borderRadius: 100,
  },
  userImage: {
    flex: 1,
    borderRadius: 100,
  },
  image: {
    resizeMode: 'contain',
    height: 140,
    width: 300,
    alignSelf: 'center',
  },
  title: {
    marginTop: 54,
    fontSize: 18,
    textAlign: 'center',
  },
  textInput: {
    flex: 1,
    paddingLeft: 48,
    paddingRight: 48,
    borderWidth: 1,
    borderColor: '#282828',
    borderRadius: 10,
    fontSize: 20,
  },
  rowGroup: {
    marginTop: 10,
    alignItems: 'center',
    flexDirection: 'row',
  },
  icon: {
    position: 'absolute',
    left: 12,
  },
  filter: {
    position: 'absolute',
    right: 12,
  },
  filterHandler: {
    marginTop: 14,
    flex: 1,
    borderWidth: 1,
    borderColor: '#282828',
    borderRadius: 10,
    padding: 20,
  },
  filterHandlerUnPress: {
    display: "none",
  },
  picker: {
    flex: 1,
  },
  banner: {
    marginTop: 20,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#C90101',
    padding: 10,
    borderRadius: 10,
  },
  text: {
    flex: 1,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginRight: 10,
  },
});
