export default fake = {
	citys: [
		{
			label: "Marilia",
			value: "marilia",
		},
		{
			label: "Pompeia",
			value: "pompeia",
		},
		{
			label: "Tupã",
			value: "tupa",
		},
	],
	works: [
		{
			label: "Pedreiro",
			value: "pedreiro",
		},
		{
			label: "Pintor",
			value: "pintor",
		},
		{
			label: "Encanador",
			value: "encanador",
		},
	]	
};