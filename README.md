<div align="center">
   
   ## 🚧 Projeto em Construção ... 🚧

   <h1>
      <a href="">
         <img src="https://gitlab.com/BDAg/ContratAnt/-/wikis/uploads/54727e43159df7ef51e93997993c6f90/ContratAnt_png_-_Cópia.png">
      </a>
   </h1>

   A ContratAnt busca permitir que pessoas encontrem um prestador de serviço adequado através de uma plataforma de facil acesso com uma experiencia de uso excelente e de forma rapida e pratica.

   [![Badge](https://img.shields.io/badge/node-v12.18.3-%237159c1?style=flat)](https://nodejs.org/en/)

   [Desenvolvedores](#%EF%B8%8F-desenvolvedores) • [Funcionalidades](#-funcionalidades) • [Tecnologias](#-tecnologias) • [Inicialização](#-inicialização) • [Contribuição](#-contribuição)

</div>

[//]: ![Banner]()

## ⌨️ Desenvolvedores

- [César Augusto Matos Ladeira](https://gitlab.com/ladeiraA)
- [Dério Lucas Meireles Cândido](https://gitlab.com/deriolucas)
- [Fernanda Maria Pereira Miranda](https://gitlab.com/FernandaM.P.M)
- [Henrique Canteiro de Oliveira](https://gitlab.com/Creaper001)
- [Lucas Vilela de Oliveira](https://gitlab.com/lucas.vilela.oliveira)
- [Pedro Henrique Martins Souza](https://gitlab.com/pedrocatins29)

## ✨ Funcionalidades

- Cadastro e login de Prestadores de Serviço
- Listagem de Profissionais
- Filtragem de Profissionais por Cidade, Nome e Categoria
- Disponibilização de Meios de Contato

## 🔨 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [GraphQL](https://graphql.org/)
- [Node.js](https://nodejs.org/en/)
- [React Native](https://reactnative.dev/)
- [TypeScript](https://www.typescriptlang.org/)
- [MySQL](https://www.mysql.com/)
- [Vercel](https://vercel.com/)

## 📦 Montagem do Ambiente do Desenvolvimento:

#### Instalando NodeJS e NPM:

- Linux

```sh
sudo apt update
sudo apt-get install nodejs
sudo apt install npm
```

- Windows 10

```sh
Baixar Instalador no site https://nodejs.org/

```

#### Instalando Visual Studio Code (este passo é opcional):

- Neste passo ocorre a instalação do editor de texto usado pelo time

```sh
https://code.visualstudio.com/download

```

#### Configurando ambiente:
- Seguir o tutorial do link abaixo de acordo com seu sistema operacional
https://reactnative.dev/docs/environment-setup


#### Iniciando Projeto:

```sh
npx react-native init "Nome do projeto"
```

### Clonar Projeto

```sh
git clone git@gitlab.com:BDAg/ContratAnt.git
```

### Rodar Projeto

- npm

```sh
npm run android
```

- Yarn

```sh
yarn android

```


## 🤝 Contribuição

Este projeto é para fins de estudo, envie menssagens dizendo o que você está fazendo e por que está fazendo, nos ensine o que você sabe

Todos os tipos de contribuições são muito bem-vindos e apreciados!

- ⭐️ Adicionar estrela ao projeto
- 🐛 Encontrar e relatar problemas
- 📥 Envie PRs para ajudar a resolver problemas ou adicionar recursos
- ✋ Influencie o futuro de ContratAnt com solicitações de recursos
